#!/usr/bin/env bash

# Activate keyboard backlight. Should be vendor agnostic.
# reference https://wiki.archlinux.org/title/Keyboard_backlight#D-Bus

# Check the max brightness value for your hardware with:
# dbus-send --type=method_call --print-reply=literal --system --dest="org.freedesktop.UPower" /org/freedesktop/UPower/KbdBacklight org.freedesktop.UPower.KbdBacklight.GetMaxBrightness

BRIGHTNESS=1
dbus-send --system --type=method_call  --dest="org.freedesktop.UPower" "/org/freedesktop/UPower/KbdBacklight" "org.freedesktop.UPower.KbdBacklight.SetBrightness" "int32:$BRIGHTNESS"